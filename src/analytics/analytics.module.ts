import { RoomModule } from './../room/room.module';
import { RoomService } from './../room/room.service';
import { WinstonModule } from 'nest-winston';
import { Module } from '@nestjs/common';
import { AnalyticsService } from './analytics.service';
import { AnalyticsController } from './analytics.controller';
import { LoggerModule, loggerConfig } from 'newrow-sbms-logger';
import { KafkaMessageModule } from 'newrow-sbms-kafka-message-pattern';

@Module({
  imports: [
    LoggerModule,
    RoomModule,
    WinstonModule.forRoot(loggerConfig),
    KafkaMessageModule,
  ],
  providers: [AnalyticsService],
  exports: [AnalyticsService],
  controllers: [AnalyticsController],
})
export class AnalyticsModule {}
