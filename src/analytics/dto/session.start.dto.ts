import { IsString, IsNumber, IsOptional } from 'class-validator';

// tslint:disable-next-line: class-name
export class sessionStartDto {
  @IsNumber()
  // tslint:disable-next-line: variable-name
  company_id: number;

  @IsNumber()
  // tslint:disable-next-line: variable-name
  room_id: any;

  @IsNumber()
  // tslint:disable-next-line: variable-name
  user_id: number;

  @IsString()
  // tslint:disable-next-line: variable-name
  user_type: string;

  @IsString()
  // tslint:disable-next-line: variable-name
  state_type: string;

  @IsOptional()
  @IsString()
  // tslint:disable-next-line: variable-name
  state_change_reason?: string;
}
