import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('tbl_analytics')
export class AnalyticsEntity extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id: number;

  @Column('tinyint', { default: 0 })
  // tslint:disable-next-line: variable-name
  is_ack: number;

  @Column('integer', { default: null })
  // tslint:disable-next-line: variable-name
  company_id: number;

  @Column('integer', { default: null })
  // tslint:disable-next-line: variable-name
  room_id: number;

  @Column('enum', {
    enum: [
      'session',
      'viewing',
      'recording',
      'fb_streaming',
      'youtube_streaming',
      'accelerator',
      'training_course',
    ],
    default: 'viewing',
  })
  // tslint:disable-next-line: variable-name
  analytic_type: string;

  @Column('integer', { default: 0 })
  // tslint:disable-next-line: variable-name
  performed_user_id: number;

  @Column('enum', {
    enum: ['user', 'guest'],
    default: 'user',
  })
  // tslint:disable-next-line: variable-name
  performed_user_type: string;

  @Column('timestamp', { default: () => 'CURRENT_TIMESTAMP' })
  // tslint:disable-next-line: variable-name
  date_started: Date;

  @Column('timestamp', { default: null })
  // tslint:disable-next-line: variable-name
  date_ended: Date;

  @Column('enum', {
    enum: ['processed', 'unprocessed', 'stuck', 'processed_as_stuck', 'failed'],
    default: 'unprocessed',
  })
  // tslint:disable-next-line: variable-name
  process_status: string;

  @Column('text', { default: null })
  // tslint:disable-next-line: variable-name
  process_fail_log: string;

  @Column('text', { default: null })
  // tslint:disable-next-line: variable-name
  message_log: string;

  @Column('varchar', { default: null })
  // tslint:disable-next-line: variable-name
  user_coordinates: string;

  @Column('varchar', { default: null })
  // tslint:disable-next-line: variable-name
  user_location_desc: string;

  @Column('text', { default: null })
  // tslint:disable-next-line: variable-name
  focus_data: string;

  @Column('text', { default: null })
  // tslint:disable-next-line: variable-name
  custom_session_metadata: string;

  @Column('text', { default: null })
  // tslint:disable-next-line: variable-name
  custom_user_session_metadata: string;
}
