import { RoomEntity } from 'src/room/room.entity';
import { RoomService } from './../room/room.service';
import { Injectable, HttpStatus } from '@nestjs/common';
import { KafkaMessageService } from 'newrow-sbms-kafka-message-pattern';
import { LoggerService } from 'newrow-sbms-logger';
import { sessionStartDto } from './dto/session.start.dto';
import { ApiService } from 'newrow-sbms-api-service';
import { InjectRepository } from '@nestjs/typeorm';
import { AnalyticsEntity } from './analytics.entity';
import { Repository } from 'typeorm';

@Injectable()
export class AnalyticsService {
  constructor(
    private readonly kafkaService: KafkaMessageService,
    private readonly roomService: RoomService,
    @InjectRepository(AnalyticsEntity)
    private readonly analyticsRepository: Repository<AnalyticsEntity>,
    private readonly logger?: LoggerService,
  ) {}

  private readonly TYPE_SESSION = 'session';
  private readonly SESSION_STATE_NEW = 'SESSION_STATE_NEW';
  private readonly SESSION_STATE_END = 'SESSION_STATE_END';
  private readonly SESSION_END_REASON_ALL_USERS_LEFT =
    'SESSION_END_REASON_ALL_USERS_LEFT';

  private async closeSession(
    stateChangeReason: string,
    roomSession:  any | RoomEntity,
  ) {
    let sessionEndOffsetSeconds: number = 0;
    let sessionEndTimestamp: number = Date.now();

    if (
      stateChangeReason === null ||
      stateChangeReason === this.SESSION_END_REASON_ALL_USERS_LEFT
    ) {
      sessionEndOffsetSeconds = 5 * 60;
    }

    sessionEndTimestamp -= sessionEndOffsetSeconds;
    roomSession.date_ended = Date.now() - sessionEndOffsetSeconds;
    
  }

  private async createAnalyticsRecord() {
    return '';
  }

  private async upadteSessionWhenNewServerStarted() {
    return '';
  }

  private async changeAnalyticsRoomSessionState(
    roomId: number,
    // tslint:disable-next-line: variable-name
    companyId: number,
    userId: number,
    userType: string,
    stateType: string,
    changeStateReason: string,
    room: object,
  ) {
    const roomSession: object = await this.analyticsRepository.find({
      where: {
        room_id: roomId,
        date_ended: null,
        analytic_type: this.TYPE_SESSION,
        order: {
          date_started: 'DESC',
        },
      },
    });

    let checkWebhook: boolean = true;
    let res: string | object = null;

    // if we found a session we close it, otherwise create it
    // close session
    if (roomSession && stateType === this.SESSION_STATE_END) {
      res = await this.closeSession();
    }
    // create new the analytics record
    else if (!roomSession && stateType === this.SESSION_STATE_NEW) {
      res = await this.createAnalyticsRecord();
    }
    // Update existing session when it started by a new server
    else if (roomSession && stateType === this.SESSION_STATE_NEW) {
      res = await this.upadteSessionWhenNewServerStarted();
    }

    // if (roomSession && checkWebhook) {
    // }

    return res;
  }

  public async sessionStart(data: sessionStartDto): Promise<object> {
    const errors: boolean | object = await this.kafkaService.validate(
      data,
      sessionStartDto,
      'Analytics session start validation',
    );
    if (errors) {
      this.logger.error('Failed to create session start', {
        description: `Cannot create session  for data :${data}`,
        message: errors,
      });
      return errors;
    }

    this.logger.info('Analytics session start', {
      description: 'analytics/session-stats/sessionStart',
      message: { data },
    });
    const { room_id, company_id, user_id, user_type, state_type } = data;

    let changeStateReason: string | null = null;
    if (data.state_change_reason) {
      changeStateReason = data.state_change_reason;
    }
    // only if the room exists we create the session
    const room: object = await this.roomService.getRoom(data.room_id);
    if (room) {
      const changeAnalyticsRoomSessionStateResult: object = await this.changeAnalyticsRoomSessionState(
        room_id,
        company_id,
        user_id,
        user_type,
        state_type,
        changeStateReason,
        room,
      );
    }
    this.logger.info('Analytics session start', {
      description: 'Room not found in analytics/session-stats/sessionStart',
      message: { data },
    });

    return ApiService.returnError(
      'query parameter is missing users property',
      'Load group messages, Validation failed',
      HttpStatus.NOT_FOUND,
    );
  }
}
