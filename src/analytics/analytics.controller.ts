import { Controller } from '@nestjs/common';
import { AnalyticsService } from './analytics.service';
import { KafkaMessagePattern } from 'newrow-sbms-kafka-message-pattern/dist';
import { sessionStartDto } from './dto/session.start.dto';

@Controller()
export class AnalyticsController {
  constructor(private readonly analyticsService: AnalyticsService) {}

  @KafkaMessagePattern('analytics/session-stats/sessionStart')
  public async analyticsSessionStart({ query, body }): Promise<object> {

    let data = query;
    if ( query.length === undefined) {
      data = body;
    }

    return  await this.analyticsService.sessionStart(data);
  }
}
