import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('tbl_rooms')
export class RoomEntity extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id: number;

  @Column('varchar', { name: 'alias', default: null })
  alias: string;

  @Column('integer', { default: null })
  // tslint:disable-next-line: variable-name
  company_id: number;

  @Column('enum', {
    enum: ['room', 'breakout'],
    default: 'room',
  })
  type: string;

  @Column('tinyint', { default: 0 })
  // tslint:disable-next-line: variable-name
  auto_upload_recordings: number;

  @Column('tinyint', { default: 0 })
  // tslint:disable-next-line: variable-name
  is_turn_configured: number;

  @Column('enum', {
    enum: ['private', 'public_org', 'public_all'],
    default: 'private',
  })
  // tslint:disable-next-line: variable-name
  privacy_level: string;

  @Column('tinyint', { default: 1 })
  // tslint:disable-next-line: variable-name
  allow_guest_enter_selfpaced: number;

  @Column('tinyint', { default: 1 })
  // tslint:disable-next-line: variable-name
  allow_guest_enter_liveclass: number;

  @Column('tinyint', { default: 1 })
  // tslint:disable-next-line: variable-name
  show_join_liveclass: number;

  @Column('integer', { default: null })
  // tslint:disable-next-line: variable-name
  active_breakout_id: number;

  @Column('integer', { default: null })
  // tslint:disable-next-line: variable-name
  file_hierarchy_id: number;

  @Column('integer', { default: null })
  // tslint:disable-next-line: variable-name
  parent_room_id: number;

  @Column('integer', { default: null })
  // tslint:disable-next-line: variable-name
  index_id: number;

  @Column('integer', { default: null })
  // tslint:disable-next-line: variable-name
  paylist_id: number;

  @Column('integer', { default: null })
  // tslint:disable-next-line: variable-name
  default_paylist_id: number;

  @Column('timestamp', { default: null })
  // tslint:disable-next-line: variable-name
  playlist_migrated: Date;

  @Column('varchar', { default: null })
  name: string;

  @Column('varchar', { default: null })
  settings: string;

  @Column('varchar', { default: null })
  // tslint:disable-next-line: variable-name
  settings_v2: string;

  @Column('varchar', { default: null })
  summary: string;

  @Column('varchar', { default: null })
  description: string;

  @Column('integer', { default: null })
  // tslint:disable-next-line: variable-name
  license_id: number;

  @Column('integer', { default: null })
  // tslint:disable-next-line: variable-name
  max_users: number;

  @Column('integer', { default: 5 })
  // tslint:disable-next-line: variable-name
  max_guests: number;

  @Column('integer', { default: null })
  // tslint:disable-next-line: variable-name
  deration: number;

  @Column('varchar', { default: null })
  avatar: string;

  @Column('float', { default: null })
  // tslint:disable-next-line: variable-name
  streaming_hours: string;

  @Column('float', { default: null })
  // tslint:disable-next-line: variable-name
  recording_hours: string;

  @Column('float', { default: null })
  // tslint:disable-next-line: variable-name
  viewing_hours: string;

  @Column('varchar', { default: null })
  // tslint:disable-next-line: variable-name
  lti_room_id: string;

  @Column('varchar', { default: null })
  // tslint:disable-next-line: variable-name
  additional_data: string;

  @Column('tinyint', { default: 0 })
  // tslint:disable-next-line: variable-name
  is_ondemand: number;

  @Column('integer', { default: null })
  // tslint:disable-next-line: variable-name
  user_modified: number;

  @Column('integer', { default: null })
  // tslint:disable-next-line: variable-name
  user_created: number;

  @Column('timestamp', { default: null })
  // tslint:disable-next-line: variable-name
  date_modified: any;

  @Column('timestamp', { default: () => 'CURRENT_TIMESTAMP' })
  // tslint:disable-next-line: variable-name
  date_created: Date;

  @Column('tinyint', { default: 0 })
  // tslint:disable-next-line: variable-name
  is_active: number;

  @Column('timestamp', { default: null })
  // tslint:disable-next-line: variable-name
  deleted: Date;

  @Column('integer', { default: null })
  // tslint:disable-next-line: variable-name
  server_id: number;

  @Column('enum', {
    enum: ['nr1', 'nr2'],
    default: 'nr1',
  })
  // tslint:disable-next-line: variable-name
  app_version: string;

  @Column('integer', { default: null })
  // tslint:disable-next-line: variable-name
  conference_id: number;
}
