import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Transport } from '@nestjs/microservices';
import { WINSTON_MODULE_NEST_PROVIDER, WinstonModule } from 'nest-winston';
import { kafkaBrokerUrls } from './utils/kafka.urls';
import 'dotenv/config';
import {
  LoggerService,
  ProdExceptionsFilter,
  loggerConfig,
} from 'newrow-sbms-logger';

const kafkaUrls: string[] = kafkaBrokerUrls(process.env.kafka_broker_urls);

// Create the microservice options object
const microserviceOptions = {
  transport: Transport.KAFKA,
  options: {
    client: {
      clientId: `analytics-${process.env.env_name}`,
      brokers: kafkaUrls,
    },
    consumer: {
      groupId: 'analytics',
    },
  },
  logger: WinstonModule.createLogger(loggerConfig),
};

async function bootstrap() {
  // Nest SQS Project init
  const app = await NestFactory.createMicroservice(
    AppModule,
    microserviceOptions,
  );

  // If NODE_ENV is prod don't close app on unhandled Exceptions
  const logger = app.get<LoggerService>(LoggerService);
  app.useGlobalFilters(new ProdExceptionsFilter(logger));

  app.enableShutdownHooks();
  app.useLogger(app.get(WINSTON_MODULE_NEST_PROVIDER));

  app.listen(() => {
    // tslint:disable-next-line:no-console
    console.log('Kafka connection established...');
  });

  // tcp connection for healthchecks
  // const server = await NestFactory.create(AppModule);

  // await server.listen(process.env.port, () => {
  //   console.log('tcp connection established');
  // });
}
bootstrap();
