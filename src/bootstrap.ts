import { getParametersFromAWS } from 'newrow-sbms-param-store';

const sharedLocalParams = {
  msName: 'analytics',
  error_code_prefix: 100,
};

const data = {

  redis_cache_url: {
    paramStore: 'redis://localhost:6379',
    local: 'redis://localhost:6379',
  },
  mysql_host: {
    paramStore: 'db.master.hostname',
    local: 'localhost',
  },
  mysql_password: {
    paramStore: 'db.password',
    local: '',
  },
  mysql_username: {
    paramStore: 'db.username',
    local: 'root',
  },

  mysql_port: {
    paramStore: 'db.port',
    local: '3306',
  },
  mysql_database: {
    paramStore: 'db.db_name',
    local: 'newrow4',
  },

  env_name: {
    paramStore: 'env_name',
    local: 'local',
  },

  ms_port: {
    paramStore: 'ms.port',
    local: 4300,
  },

  kafka_broker_urls: {
    paramStore: 'kafka_broker_urls',
    local: 'kafka.dev.internal.newrow:9092',
  },

};

const apiVersion: string = 'latest';
const region: string = 'us-east-1';
const accessKeyId: string = 'Some_Key';
const secretAccessKey: string = 'SOME_TOKEN';
const sessionToken: string = 'SOME_SESSION_TOKEN';

getParametersFromAWS(
  data,
  sharedLocalParams,
  apiVersion,
  region,
  accessKeyId,
  secretAccessKey,
  sessionToken,
);
