import { TypeOrmModule } from '@nestjs/typeorm';
import { Module, CacheModule, CacheInterceptor } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { WinstonModule } from 'nest-winston';
import { loggerConfig, LoggerModule } from 'newrow-sbms-logger';
import { RedisActionsModule } from 'newrow-sbms-redis';
import { RedisModule } from 'nestjs-redis';
import { ConfigModule } from '@nestjs/config';
import { APP_INTERCEPTOR } from '@nestjs/core';
import * as redisStore from 'cache-manager-redis-store';
import 'dotenv/config';
import { TerminusModule } from '@nestjs/terminus';
import { TerminusOptionsService } from './utils/health/terminus-options.service';
import { AnalyticsModule } from './analytics/analytics.module';
import { RoomModule } from './room/room.module';

@Module({
  imports: [
    LoggerModule,
    WinstonModule.forRoot(loggerConfig),
    RedisModule.register({ url: process.env.redis_cache_url }),
    RedisActionsModule,
    ConfigModule.forRoot({ isGlobal: true }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.mysql_host,
      port: Number(process.env.mysql_port),
      username: process.env.mysql_username,
      password: process.env.mysql_password,
      database: process.env.mysql_database,
      entities: ['dist/**/*.entity{.ts,.js}'],
      synchronize: false,
    }),
    // CacheModule.register({
    //   store: redisStore,
    //   host: process.env.redis_cache_url.slice(
    //     process.env.redis_cache_url.indexOf('://') + 3,
    //     process.env.redis_cache_url.lastIndexOf(':'),
    //   ),
    //   port: process.env.redis_cache_url.slice(
    //     process.env.redis_cache_url.lastIndexOf(':') + 1,
    //   ),
    // }),
    TerminusModule.forRootAsync({
      useClass: TerminusOptionsService,
    }),
    AnalyticsModule,
    RoomModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    // {
    //   provide: APP_INTERCEPTOR,
    //   useClass: CacheInterceptor,
    // },
  ],
})
export class AppModule {}
