import { AnalyticsService } from './analytics/analytics.service';
import {
  Controller,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { AppService } from './app.service';
import {
  MessagePattern,
  Payload,
  KafkaContext,
  Ctx,
} from '@nestjs/microservices';
import { LoggerInterceptor } from 'newrow-sbms-logger';
import { getAppropriateFunction } from 'newrow-sbms-kafka-message-pattern';
import { RoomService } from './room/room.service';

@Controller()
@UseInterceptors(LoggerInterceptor)
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly analyticsService: AnalyticsService,
    private readonly roomService: RoomService,
  ) {
    this.runAppropriateFunction = this.runAppropriateFunction.bind(this);
  }

  @MessagePattern('analytics')
  async runAppropriateFunction(@Payload() message: any) {
    const { value } = message;
    const { body, actionType } = value;

    if (actionType) {
      return getAppropriateFunction(actionType).call(this, body);
    }
  }
}
