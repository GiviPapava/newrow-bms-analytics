export const kafkaBrokerUrls = (urls: string): string[] => {
  return urls.split(',');
};
