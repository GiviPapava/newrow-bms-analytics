import {
  TerminusEndpoint,
  TerminusOptionsFactory,
  DNSHealthIndicator,
  MicroserviceHealthIndicator,
  TerminusModuleOptions,
} from '@nestjs/terminus';
import { Injectable } from '@nestjs/common';
import { Transport } from '@nestjs/common/enums/transport.enum';

@Injectable()
export class TerminusOptionsService implements TerminusOptionsFactory {
  constructor(
    private readonly dns: DNSHealthIndicator,
    private readonly microservice: MicroserviceHealthIndicator,
  ) {}

  createTerminusOptions(): TerminusModuleOptions {
    const healthEndpoint: TerminusEndpoint = {
      url: '/health',
      healthIndicators: [
        async () => this.dns.pingCheck('newrow', 'https://newrow.com'),
        async () =>
          this.microservice.pingCheck('redis', {
            transport: Transport.REDIS,
            options: { url: process.env.redis_cache_url },
          }),
      ],
    };
    return {
      endpoints: [healthEndpoint],
    };
  }
}
