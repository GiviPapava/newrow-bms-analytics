import { Catch, ArgumentsHost } from '@nestjs/common';
import { BaseRpcExceptionFilter } from '@nestjs/microservices';
import 'dotenv/config';
import { Observable, throwError } from 'rxjs';
import { LoggerService } from 'newrow-sbms-logger';

@Catch()
export class ProdExceptionsFilter extends BaseRpcExceptionFilter {
  constructor(private readonly loggerService: LoggerService) {
    super();
  }

  catch(exception: any, host: ArgumentsHost): Observable<any> {
    // Get topic
    // const hostArgs = host.getArgByIndex(0);
    // const { topic } = hostArgs;

    this.loggerService.error('Exception', {
      Error: JSON.stringify(exception, Object.getOwnPropertyNames(exception)),
      host,
    });

    if (process.env.NODE_ENV === 'prod') {
      return new Observable();
    }
    return throwError(exception.getError());
  }
}
