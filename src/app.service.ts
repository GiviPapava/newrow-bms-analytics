import { Injectable } from '@nestjs/common';
import { RedisActionsService } from 'newrow-sbms-redis';
import { LoggerService } from 'newrow-sbms-logger';

@Injectable()
export class AppService {
  constructor(
    private readonly redisService: RedisActionsService,
    private readonly loggerService: LoggerService,
  ) {}

  getHello({ data }): string {
    this.loggerService.error('message worked', {
      object: 'some object',
      smething: 'something else',
    });

    let date = Date.now();
    const end = Date.now() + 1000;
    /* Long Job Operation Simulation */
    // while (date < end) {
    //   date = Date.now();
    // }

    const result = JSON.stringify({
      gateway: data,
      microService: 'boilerplate-1',
    });
    return result;
  }
}
